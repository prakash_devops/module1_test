def calculate(operator, number1, number2):
    if operator == '+':
        return number1 + number2;
    elif operator == '-':
        return number1 - number2;
    elif operator == 'x':
        return number1 * number2;    
    elif operator == '/':
        return number1 / number2; 

def nextLineToRead(values):
    if len(values) == 2:
        #print(f'goto value {values[1]} and its type {type(values[1])}')
        next_line = int(values[1])
        #print(f'goto_line value {next_line}')
    elif len(values) == 5:
        next_line = int(calculate(values[2], int(values[3]), int(values[4])))
    return next_line

with open("part1/goto_calculator_input.txt", "r") as read_file:
    input_data = read_file.read().splitlines()
    print(f'No. of lines {len(input_data)}')
    print(type(input_data))

unique_values = list()
line = input_data[0]
while True:
    values = line.split()
    if line in unique_values:
        print(f'line found {line}' )
        break
    next = nextLineToRead(values)
    unique_values.append(line)
    line = input_data[next -1]
    
    