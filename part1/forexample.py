import math
for letter in 'Human': 
    print (letter)

l_letters = [letter for letter in 'Human'] 
print (l_letters)   

square_odd_numbers = [x*x for x in range(1,20) if(x%2 != 0)]
print(square_odd_numbers)

def square_root():
    for i in range(10):
        print(f'Square root of {i} is {math.sqrt(i)}')

square_root()       


pythagorean_triples = [
    (x,y) for x in range (1,10)
    for y in range (1,10)
    if math.sqrt(x*x + y*y).is_integer()
    ]

print(f'Square root of 3,4 is {math.sqrt(25)}')
print(pythagorean_triples)