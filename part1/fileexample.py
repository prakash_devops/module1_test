f = open("test.txt", "w")
f.write('first line\n')
f.write('next line\n')
f.write('third line\n')
f.close()

with open("test.txt", "r") as f:
    text_str = f.read()

print(text_str)
