with open("part1/calculator_input.txt", "r") as calc_input:
    input_data = calc_input.read().splitlines()
    print(type(input_data))
    print(len(input_data))

def calculate(operator, number1, number2):
    if operator == '+':
        return number1 + number2;
    elif operator == '-':
        return number1 - number2;
    elif operator == 'x':
        return number1 * number2;    
    elif operator == '/':
        return number1 / number2;    
      
result = 0
for line in input_data:
   values = line.split()  
   #print(values) 
   result += calculate(values[1], int(values[2]), int(values[3]))
   
print(f'Total Result: {result}'  )