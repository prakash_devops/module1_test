def calculate(operator, number1, number2):
    if operator == '+':
        return number1 + number2;
    elif operator == '-':
        return number1 - number2;
    elif operator == 'x':
        return number1 * number2;    
    elif operator == '/':
        return number1 / number2; 

def nextLineToRead(line_number, line, input_data):
    values = line.split()
    if values[1] == 'calc':
        next_line = int(calculate(values[2], int(values[3]), int(values[4])))
       
    elif values[0] == 'replace':
        line_from = int(values[1])
        line_to = int(values[2])
        copy_line = input_data[line_to-1]
        input_data.pop(line_from -1)
        input_data.insert(line_from-1, copy_line)
        next_line = line_number + 1

    elif values[0] == 'remove':    
        line_tobe_removed = int(values[1])
        input_data.pop(line_tobe_removed -1)
        if(line_tobe_removed <= line_number):
            next_line = line_number
        else:        
            next_line = line_number + 1
    else:     
        next_line = int(values[1])
    return next_line

with open("part1/step_4_input.txt", "r") as read_file:
    input_data = read_file.read().splitlines()
    print(f'No. of lines {len(input_data)}')
    print(type(input_data))

unique_values = set()
line_number = 1
line = input_data[0]
while True:
    print(f'line: {line}')
    if line in unique_values:
        print(f'line found {line}' )
        break
    line_number = nextLineToRead(line_number, line, input_data)
    unique_values.add(line)
    line = input_data[line_number -1]
    
    