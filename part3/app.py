from flask import Flask
from flask import render_template
from flask import request
app = Flask(__name__)
@app.route('/hello')
def hello_world():
    return 'Hello World!'

@app.route('/prakash' )
def hello_world1():
    return 'Hello Mr Prakash Welcome to Python World!'    

books = [
    {'id':1, 'title':'Clean Code', 'authors':'Robert C. Martin'},
    {'id':2, 'title':'The Devops Handbook', 'authors':'Gene Kim, Jez Humble, Patrick Debois, John Willis'},
    {'id':3, 'title':'The Phoenix Project', 'authors':'Gene Kim, Kevein Behr, George Spafford'}
] 

@app.route('/books')  
def get_books():
    return render_template('book_list.html',books=books)

@app.route('/films/list')  
def get_films():
    return render_template('films.html')    

with open("templates/films.csv", "r") as f:
    films_data = f.read().splitlines()
    print(f'Type: {type(films_data)} and Size: {len(films_data)}')
@app.route('/films/table')  
def get_films_table():
    return render_template('films_table.html', films=films_data)        

@app.route('/show-messages')    
def echo():
    name=request.values.get("name", "")
    message=request.values.get("message", "")
    return f'Hey {name} , you said {message}'